PCBNEW-LibModule-V1  Fri 20 Jun 2014 13:07:38 CEST
# encoding utf-8
Units mm
$INDEX
DF12E-50DP-0.5V
LQFP48
$EndINDEX
$MODULE DF12E-50DP-0.5V
Po 0 0 0 15 53A407E1 00000000 ~~
Li DF12E-50DP-0.5V
Sc 0
AR /53174EB7
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "U1001"
T1 0 -2.54 1 1 0 0.15 N V 21 N "MG323"
DS -7.4 5.6 7.4 5.6 0.15 21
DS 7.4 5.6 7.4 1.4 0.15 21
DS 7.4 1.4 -7.4 1.4 0.15 21
DS -7.4 1.4 -7.4 5.6 0.15 21
$PAD
Sh "3" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "/SIM_VCC"
Po -5.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "5" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/SIM_IO"
Po -5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "7" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "/SIM_RST"
Po -4.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "9" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "11" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po -3.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "13" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "15" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "17" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "19" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "21" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "23" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "25" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 15 "N-000002"
Po 0 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "27" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "29" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 6 "/UART_RXD"
Po 1 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "31" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "33" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 7 "/UART_TXD"
Po 2 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "35" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 16 "N-0000054"
Po 2.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "37" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "39" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "41" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po 4 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "43" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po 4.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "45" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po 5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "47" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po 5.5 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "49" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po 6 1.8
.LocalClearance 0.1
$EndPAD
$PAD
Sh "2" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "4" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "6" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "8" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "10" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "12" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "14" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "16" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "18" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "GND"
Po -2 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "20" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 17 "N-0000058"
Po -1.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "22" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 18 "N-0000062"
Po -1 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "24" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 9 "/UART_~DCD~"
Po -0.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "26" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "28" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 8 "/UART_~CTS~"
Po 0.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "30" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "32" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 11 "/UART_~DTR~"
Po 1.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "34" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 12 "/UART_~RTS~"
Po 2 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "36" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 10 "/UART_~DSR~"
Po 2.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "38" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 5 "/UART_RING"
Po 3 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "40" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "42" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/VBat"
Po 4 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "44" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/VBat"
Po 4.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "46" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/VBat"
Po 5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "48" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/VBat"
Po 5.5 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "50" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/VBat"
Po 6 5.2
.LocalClearance 0.1
$EndPAD
$PAD
Sh "1" R 0.3 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "/SIM_CLK"
Po -6 1.8
.LocalClearance 0.1
$EndPAD
$EndMODULE DF12E-50DP-0.5V
$MODULE LQFP48
Po 0 0 0 15 53A415BF 00000000 ~~
Li LQFP48
Sc 0
AR /5318670B
Op 0 0 0
T0 0 1.27 1.27 1.27 0 0.2 N V 21 N "U1003"
T1 0 -0.635 1.27 1.27 0 0.2 N V 21 N "LPC134X-FBD48"
DS -3.5 3.5 -3.5 3.5 0.2 21
DC -2.5 -2.5 -2.1 -2.5 0.2 21
DS -3.5 3.5 -3.5 -3.5 0.2 21
DS -3.5 -3.5 -3.5 -3.5 0.2 21
DS -3.5 -3.5 3.5 -3.5 0.2 21
DS 3.5 -3.5 3.5 -3.5 0.2 21
DS 3.5 -3.5 3.5 3.5 0.2 21
DS 3.5 3.5 3.5 3.5 0.2 21
DS 3.5 3.5 -3.5 3.5 0.2 21
$PAD
Sh "7" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 26 "/Xtal_out"
Po -4.3 0.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "6" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 25 "/Xtal_in"
Po -4.3 -0.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "8" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "/3.0V"
Po -4.3 0.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "5" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 27 "GND"
Po -4.3 -0.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "4" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 7 "/PIO0_1"
Po -4.3 -1.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "3" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 6 "/MCU_~RESET~"
Po -4.3 -1.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "2" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.3 -2.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "1" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.3 -2.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "9" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.3 1.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "10" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.3 1.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "11" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 31 "N-000007"
Po -4.3 2.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "12" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 30 "N-000006"
Po -4.3 2.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "30" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/GSM_RESET"
Po 4.25 0.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "31" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 28 "N-000004"
Po 4.25 -0.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "29" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 12 "/SWCLK"
Po 4.25 0.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "28" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "/SWO"
Po 4.25 1.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "27" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 11 "/SERVO_PWM"
Po 4.25 1.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "26" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 2.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "25" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 2.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "32" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 8 "/SERVO_GP0"
Po 4.25 -0.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "33" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 9 "/SERVO_GP1"
Po 4.25 -1.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "34" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 10 "/SERVO_GP2"
Po 4.25 -1.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "35" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 -2.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "36" R 1.2 0.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 21 "/UART_~DTR~"
Po 4.25 -2.75
.LocalClearance 0.1
$EndPAD
$PAD
Sh "42" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 3 "/GSM_TERM_ON"
Po 0.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "41" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 27 "GND"
Po 0.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "40" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "39" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 13 "/SWDIO"
Po 1.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "38" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "37" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 20 "/UART_~DSR~"
Po 2.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "43" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 19 "/UART_~DCD~"
Po -0.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "44" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 1 "/3.0V"
Po -0.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "45" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 22 "/UART_~RTS~"
Po -1.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "46" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 16 "/UART_RXD"
Po -1.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "47" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 17 "/UART_TXD"
Po -2.25 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "48" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 15 "/UART_RING"
Po -2.75 -4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "19" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 4 "/MCU_USB_DM"
Po 0.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "20" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 5 "/MCU_USB_DP"
Po 0.75 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "21" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "22" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 23 "/USB_CONNECT"
Po 1.75 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "23" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 18 "/UART_~CTS~"
Po 2.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "24" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 29 "N-000005"
Po 2.75 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "18" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "17" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "16" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "15" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.75 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "14" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 24 "/USB_VBus"
Po -2.25 4.25
.LocalClearance 0.1
$EndPAD
$PAD
Sh "13" R 1.2 0.3 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.75 4.25
.LocalClearance 0.1
$EndPAD
$SHAPE3D
Na "lqfp/lqfp48.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE LQFP48
$EndLIBRARY
