/**
 * @file	gsm_cmdline.c
 * @brief	
 * @date	Jun 26, 2014
 * @author	Jan Tusil <jenda.tusil@gmail.com>
 */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <gsm_cmdline.h>

void GsmCmdLine_init(GsmCmdLine_t *self)
{
	self->used = 0;
}

void GsmCmdLine_clear(GsmCmdLine_t *self)
{
	self->buffer[0] = '\0';
	self->used = 0;
}

bool GsmCmdLine_append_cmd(GsmCmdLine_t *self, const char *cmd)
{
	size_t len = strlen(cmd);
	// TODO: Calculate again
	if (len + self->used + 2 > GSM_CMDLINE_BUFFER_LEN)
		return false;

	if (self->used != 0)
		self->buffer[self->used++] = ';';
	else {
		strcpy(self->buffer, "AT");
		self->used = 2;
	}

	strcpy(self->buffer + self->used, cmd);
	self->used += len;
	return true;
}

bool GsmCmdLine_append_fcmd(GsmCmdLine_t *self, const char *fmt, ...)
{
	va_list args;
	char buff[100];

	va_start(args, fmt);
	vsprintf(buff, fmt, args);
	va_end(args);

	return GsmCmdLine_append_cmd(self, buff);
}
