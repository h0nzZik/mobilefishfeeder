/**
 * @file	priv_GsmModule.h
 * @brief	
 * @date	Jun 18, 2014
 * @author	jenda
 */

#ifndef PRIV_GSMMODULE_H_
#define PRIV_GSMMODULE_H_

#include <stdbool.h>
#include <stdint.h>

#include "ByteBuffer.h"
#include "gsm_cmdline.h"

#include "GsmModule.h"

#define GSM_MODULE_SERIAL_BUFFER_SIZE	32
#define GSM_INPUT_CMDLINE_LEN			128

enum input_cmdline_st
{
	// Keep this order!
	INCMDST_EMPTY,
	INCMDST_READING,
	INCMDST_CR,
	INCMDST_FINISHED
};

typedef struct
{
	// TODO: Watchdog READING state (max 1 sec?)
	enum input_cmdline_st state;
	char cmdline[GSM_INPUT_CMDLINE_LEN + 1];
	size_t have;
} GsmInputLine_t;

void GsmInputLine_init(GsmInputLine_t *self);
void GsmInputLine_putchar(GsmInputLine_t *self, char character);
void GsmInputLine_clear(GsmInputLine_t *self);


typedef bool (*GsmReply_cb)(GsmModule_t *module, void *data);


enum e_GsmModule_st
{
	EGSM_ST_INITIAL,
	EGSM_ST_TERM_ON_CYCLE,
	EGSM_ST_WAIT_SYSSTART,
	EGSM_ST_INITIAL_SEQUENCE,
	EGSM_ST_ON
};

struct GSM_MODULE
{
	enum e_GsmModule_st st;
	uint16_t ms_timer;			// Maximal value is one minute

	uint32_t
		fl_sysstart			: 1,
		/**
		 * @brief
		 */
		fl_cb_only_lines	: 1,
		fl_reserved			: 30;

	void *serial_write_ctx;
	bool (*serial_write_data)(void *ctx, uint8_t *data, size_t len);

	void *serial_read_ctx;
	size_t (*serial_read_data)(void *ctx, uint8_t *data, size_t maxlen);


	// Should be filled from UART
	ByteBuffer_t input_buffer;
	uint8_t input_buffer_data[GSM_MODULE_SERIAL_BUFFER_SIZE];	// Do not access directly

	GsmCmdLine_t output_cmdline;

	GsmInputLine_t input_cmdline;

	// Reply callback. Returns true if line was handled
	GsmReply_cb reply_callback;
	//bool (*reply_callback)(GsmModule_t *module, void *data);
	void *reply_callback_data;

	char unsolicited_cmdline[GSM_INPUT_CMDLINE_LEN + 1];

};


#endif /* PRIV_GSMMODULE_H_ */
