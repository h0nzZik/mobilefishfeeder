/**
 * @file	pinout.c
 * @brief	
 * @date	Jun 27, 2014
 * @author	jenda
 */

#include <stdbool.h>

#include <lpc13uxx_gpio.h>

#include "pinout.h"



#define GSM_RESET_PORT			0
#define GSM_RESET_PIN			22

#define GSM_TERM_ON_PORT		0
#define GSM_TERM_ON_PIN			23

static inline void port_output(int port, int pin, int value)
{
	if (value)
		GPIO_SetValue(port, pin);
	else
		GPIO_ClearValue(port, pin);
}

void pin_gsm_term_on(bool value)
{
	port_output(GSM_TERM_ON_PORT, GSM_TERM_ON_PIN, value);
}

void pin_gsm_term_on_setup(void)
{
	GPIO_SetDir(GSM_TERM_ON_PORT, GSM_TERM_ON_PIN, 1);
}

void pin_gsm_reset(bool value)
{
	port_output(GSM_TERM_ON_PORT, GSM_TERM_ON_PIN, value);
}


void pinout_init(void)
{
	GPIOInit();
	GPIO_SetDir(GSM_RESET_PORT, GSM_RESET_PIN, 1);
}
