/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC13Uxx.h"
#endif

#include <stdio.h>

#include <lpc13uxx_uart.h>

#include <cr_section_macros.h>

#include "pinout.h"
#include "App.h"

#include "priv_GsmModule.h"

// TODO: insert other include files here

// TODO: insert other definitions and declarations here


GsmModule_t gsm_module;


void SysTick_Handler(void)
{
	GsmModule_ms_tick(&gsm_module);
}

bool init(void)
{
	pinout_init();
	// Init UART. TODO: Maybe it should be swapped?
	uart_byte_buffer = NULL;
	ModemInit();
	UARTInit(115200);
	GsmModule_init_early(&gsm_module);
	uart_byte_buffer = & gsm_module.input_buffer;

	if (SysTick_Config(SystemCoreClock / 1000))
		app_fatal_error("Can not setup SysTick");

	return true;
}

int main(void)
{

    // TODO: insert code here

	printf("Hello world\n");

	init();

    while(1) {
    	GsmModule_iter(&gsm_module);
    }
    return 0 ;
}
