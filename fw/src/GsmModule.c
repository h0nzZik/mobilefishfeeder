/**
 * @file	GsmModule.c
 * @brief	
 * @date	Jun 18, 2014
 * @author	jenda
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdarg.h>

#include "pinout.h"

#include "priv_GsmModule.h"

#define DEFAULT_BAUD_RATE 115200

static void GsmModule_sendCMD(GsmModule_t *self, const char *cmd)
{
	// TODO: Not implemented yet
}

#if 0
bool GsmModule_getNetworks(GsmModule_t *self)
{
	assert(NULL != self);

	return GsmModule_sendCMD(self, "AT+WS46=?\r");
}

bool GsmModule_setNetwork(GsmModule_t *self, int network)
{
	assert(NULL != self);

	return GsmModule_sendCMD(self, "AT+WS46=%d\r", network);
}

bool GsmModule_readPwdRq(GsmModule_t *self)
{
	assert(NULL != self);

	return GsmModule_sendCMD(self, "AT+CPIN?\r");
}

bool GsmModule_enterPin(GsmModule_t *self, const char *pin)
{
	assert(NULL != self);
	assert(NULL != pin);

	return GsmModule_sendCMD(self, "AT+CPIN=%s\r", pin);
}

const char * GsmModule_getReply(GsmModule_t *self)
{
	return "";
}

bool GsmModule_SMSRead_isSupported(GsmModule_t *self)
{
	assert(NULL != self);
	bool rv = GsmModule_sendCMD(self, "AT+CMGR=?");

	char *reply = GsmModule_getReply(self);

	if (!strcmp(reply, "\r\nOK\r\n"))
		return true;

	return false;
}

bool GsmModule_SMSRead(GsmModule_t *self, int idx, SMS_t *sms)
{
	assert(NULL != self);

	bool rv = GsmModule_sendCMD(self, "AT+CMGR=%d\r", idx);
	char *reply = GsmModule_getReply(self);

	// PDU mode

	// Header did not match. Error
	if (strncmp(reply, "\r\n+CMGR:", 8))
		return false;
}

bool GsmModule_setSerialHWControl(GsmModule_t *self)
{
	assert(NULL != self);

	return GsmModule_sendCMD(self, "AT\\Q%d", 3);
}


bool GsmModule_inicializeHW(GsmModule_t *self)
{
	bool rv = true;

	rv = rv && GsmModule_setSerialHWControl(self);
}

#endif


void GsmInputLine_init(GsmInputLine_t *self)
{
	self->state = INCMDST_EMPTY;
	self->have = 0;
}

void GsmInputLine_clear(GsmInputLine_t *self)
{
	self->state = INCMDST_EMPTY;
	self->have = 0;
}

void GsmInputLine_putchar(GsmInputLine_t *self, char character)
{
	if (self->have + 1 == GSM_INPUT_CMDLINE_LEN)
	{
		/* TODO: Warn overflow */
		/* TODO: One state for this to wait until newline */
		GsmInputLine_clear(self);
		return;
	}

	assert(self->state < INCMDST_FINISHED);
	self->cmdline[self->have++] = character;


	switch(self->state){
	case INCMDST_EMPTY:
		self->state = INCMDST_READING;
		/* fall through */
	case INCMDST_READING:
		// CR
		if (character == '\r')
			self->state = INCMDST_CR;
		break;

	case INCMDST_CR:
		// LF
		if (character == '\n')
		{
			self->cmdline[self->have] = '\0';
			self->state = INCMDST_FINISHED;
		} else {
			// TODO: Warn something
			GsmInputLine_clear(self);
		}
		break;

	default:
		/* should not hapen*/
		assert(0);
		break;
	}
}

bool GsmInputLine_have_line(const GsmInputLine_t *self)
{
	return (self->state == INCMDST_FINISHED);
}

bool GsmInputLine_is_emptyline(const GsmInputLine_t *self)
{
	assert(self->state == INCMDST_FINISHED);

	return !strcmp(self->cmdline, "\r\n");
}

bool GsmInputLine_match_prefix(const GsmInputLine_t *self, const char *prefix)
{
	assert(self->state == INCMDST_FINISHED);

	size_t prefixlen = strlen(prefix);

	if (prefixlen < self->have)
		return false;

	return !strncmp(self->cmdline, prefix, prefixlen);
}

bool GsmModule_init_early(GsmModule_t *self)
{
	GsmInputLine_init(&self->input_cmdline);
	ByteBuffer_init(&self->input_buffer, self->input_buffer_data, GSM_MODULE_SERIAL_BUFFER_SIZE);
	GsmCmdLine_init(&self->output_cmdline);

	self->st = EGSM_ST_INITIAL;
	self->ms_timer = 250;
	return true;
}

void GsmModule_read(GsmModule_t *self)
{
	assert(self->input_cmdline.state != INCMDST_FINISHED);

	while (!ByteBuffer_empty(&self->input_buffer) && self->input_cmdline.state != INCMDST_FINISHED)
	{
		char c = ByteBuffer_out(&self->input_buffer);
		GsmInputLine_putchar(&self->input_cmdline, c);
	}
}

/**
 * @returns true if an unsolicited message was handled
 */
bool GsmModule_handle_unsolicited(GsmModule_t *self)
{

	if (GsmInputLine_is_emptyline(&self->input_cmdline))
	{
		GsmInputLine_clear(&self->input_cmdline);
		return true;
	}

	if (GsmInputLine_match_prefix(&self->input_cmdline, "^SYSSTART"))
	{
		GsmInputLine_clear(&self->input_cmdline);
		self->fl_sysstart = 1;

		return true;
	}

	return false;
}

bool GsmModule_replyCallback_register(GsmModule_t *self, GsmReply_cb callback, void *data)
{
	if (self->reply_callback != NULL)
	{
		return false;
	}

	self->reply_callback = callback;
	self->reply_callback_data = data;
	return true;
}

bool GsmModule_echo_off_cb(GsmModule_t *self, void *data)
{

	return false;
}

bool GsmModule_echo_off(GsmModule_t *self)
{
	// Register callback
	if (!GsmModule_replyCallback_register(self, GsmModule_echo_off_cb, NULL))
		return false; // Not started

	GsmCmdLine_append_cmd(&self->output_cmdline, "ATE0");
	return true;
}


bool GsmModule_connect_cb(GsmModule_t *self, void *data)
{
	// TODO: Handle timeouts

	// Wait until we have full line
	if (!GsmInputLine_have_line(&self->input_cmdline))
	{
		// Not handled because we do not have data
		// This function will be registered automatically
		return false;
	}

	if (GsmInputLine_is_emptyline(&self->input_cmdline))
	{
		// Ignore empty line. It will be handled by unsolicited handler
		// It is a bit hacky
		return false;
	}

	// This is the reply we was waiting for. Now we can do something more useful.
	if (GsmInputLine_match_prefix(&self->input_cmdline, "OK"))
	{
		if (self->st == EGSM_ST_INITIAL_SEQUENCE)
		{
			// Turns echo off
			bool rv = GsmModule_echo_off(self);
			// Because caller unregistered us before called us
			assert(rv == true);
		}
		// We have handled this cmdline.
		// Caller should not overwrite registered callback
		return true;
	}

	return false;	// Not handled
}

bool GsmModule_connect(GsmModule_t *self)
{
	// Register callback
	if (!GsmModule_replyCallback_register(self, GsmModule_connect_cb, NULL))
		return false; // Not started


	GsmCmdLine_append_cmd(&self->output_cmdline, "");	// This should create empty AT command

	return true; // Started
}

/**
 * @return true => can continue
 */
static bool GsmModule_handle_startup(GsmModule_t *self)
{
	switch(self->st)
	{
	case EGSM_ST_INITIAL:
		if (self->ms_timer)
			return false;
		// Now we can start the module
		pin_gsm_term_on_setup();
		pin_gsm_term_on(0);
		self->st = EGSM_ST_TERM_ON_CYCLE;
		self->ms_timer = 1000;
		return false;

	case EGSM_ST_TERM_ON_CYCLE:
		if (self->ms_timer)
			return false;
		pin_gsm_term_on(1);
		self->st = EGSM_ST_WAIT_SYSSTART;
		break;

	case EGSM_ST_WAIT_SYSSTART:
		if (self->fl_sysstart)
		{
			self->fl_sysstart = 0;
			self->st = EGSM_ST_INITIAL_SEQUENCE;
		}
		break;

	case EGSM_ST_INITIAL_SEQUENCE:
		GsmModule_connect(self);
		break;

	case EGSM_ST_ON:

		break;
	}

	return true;
}

bool GsmModule_iter(GsmModule_t *self)
{

	if (!GsmModule_handle_startup(self))
		return false;

	// First read serial port (or buffered data)
	// Push it to line buffer (if possible)
	GsmModule_read(self);

	// Clear command line
	GsmCmdLine_clear(&self->output_cmdline);

	/* Waiting for reply */
	if (self->reply_callback != NULL)
	{
		// Handle reply (and maybe fill command line)
		// Note that reply callback may use input line which is not already read

		// Disable it and call
		GsmReply_cb reply_callback =  self->reply_callback;
		self->reply_callback = NULL;

		bool rv = reply_callback(self, self->reply_callback_data);

		// Line was not handled.
		if (rv == false)
		{
			assert(self->reply_callback == NULL);
			self->reply_callback = reply_callback;

			// Not handled and have full line -> Move to unsolicited message queue

			if (self->input_cmdline.state == INCMDST_FINISHED)
			{
				strcpy(self->unsolicited_cmdline, self->input_cmdline.cmdline);
			}
		}

		if (self->input_cmdline.state == INCMDST_FINISHED)
		{
			// Line was handled (or moved) -> clear it
			GsmInputLine_clear(&self->input_cmdline);
		}
	} else {
		// Not waiting for reply. If data line present, move it to unsolicited queue

		// Fill command line
	}

	// Handle unsolicited messages
	GsmModule_handle_unsolicited(self);

	self->unsolicited_cmdline[0] = '\0';

	// TODO: Send command line
	if (self->output_cmdline.buffer[0] != '\0')
	{
		GsmModule_sendCMD(self, self->output_cmdline.buffer);
		GsmCmdLine_clear(&self->output_cmdline);
	}

	return true;	// No extra meaning
}

bool GsmModule_ready(GsmModule_t *self)
{
	return self->st == EGSM_ST_ON;
}

void GsmModule_ms_tick(GsmModule_t *self)
{
	if (self->ms_timer)
		self->ms_timer--;
}
