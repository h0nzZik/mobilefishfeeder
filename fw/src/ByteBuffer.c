/**
 * @file	ByteBuffer.c
 * @brief	
 * @date	Jun 27, 2014
 * @author	jenda
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "ByteBuffer.h"


ByteBuffer_t ByteBuffer_initial(uint8_t *buf, size_t size)
{
	const ByteBuffer_t initial =
	{ .size = size, .in = 0, .out = 0, .buf = buf };

	return initial;
}
void ByteBuffer_init(ByteBuffer_t *self, uint8_t *buf, size_t size)
{
	const ByteBuffer_t tmp =
	{
		.size = size,
		.in = 0,
		.out = 0,
		.buf = buf
	};

	memcpy(self, &tmp, sizeof(*self));
}

bool ByteBuffer_empty(const ByteBuffer_t *self)
{
	return self->in == self->out;
}

bool ByteBuffer_full(const ByteBuffer_t *self)
{
	return (self->out == self->in + 1) || ((self->in + 1 == self->size) && (self->out == 0));
}

void ByteBuffer_in(ByteBuffer_t *self, uint8_t value)
{
	assert(!ByteBuffer_full(self));

	self->buf[self->in++] = value;

	if (self->in == self->size)
		self->in = 0;
}

uint8_t ByteBuffer_out(ByteBuffer_t *self)
{
	assert(!ByteBuffer_empty(self));

	uint8_t ret = self->buf[self->out++];
	if (self->out == self->size)
		self->out = 0;
	return ret;
}

void ByteBuffer_flush(ByteBuffer_t *self)
{
	self->out = self->in;
}
