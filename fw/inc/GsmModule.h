/**
 * @file	GsmModule.h
 * @brief	
 * @date	Jun 18, 2014
 * @author	jenda
 */

#ifndef GSMMODULE_H_
#define GSMMODULE_H_

struct GSM_MODULE;

typedef struct GSM_MODULE GsmModule_t;

#ifdef __cplusplus
extern "C" {
#endif

bool GsmModule_init_early(GsmModule_t *self);
void GsmModule_ms_tick(GsmModule_t *self);
bool GsmModule_iter(GsmModule_t *self);
bool GsmModule_ready(GsmModule_t *self);

#ifdef __cplusplus
}
#endif


typedef struct
{

	size_t len;
	char text[300];	// TODO: How big is it?
} SMS_t;

#endif /* GSMMODULE_H_ */
