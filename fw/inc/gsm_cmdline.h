/**
 * @file	gsm_cmdline.h
 * @brief	
 * @date	Jun 26, 2014
 * @author	jenda
 */

#ifndef GSM_CMDLINE_H_
#define GSM_CMDLINE_H_

#define GSM_CMDLINE_BUFFER_LEN			128

typedef struct
{
	char buffer[GSM_CMDLINE_BUFFER_LEN];
	uint16_t used;
} GsmCmdLine_t;

void GsmCmdLine_init(GsmCmdLine_t *self);
bool GsmCmdLine_append_cmd(GsmCmdLine_t *self, const char *cmd);
bool GsmCmdLine_append_fcmd(GsmCmdLine_t *self, const char *fmt, ...);
void GsmCmdLine_clear(GsmCmdLine_t *self);

#endif /* GSM_CMDLINE_H_ */
