/**
 * @file	ByteBuffer.h
 * @brief	
 * @date	Jun 27, 2014
 * @author	jenda
 */

#ifndef BYTEBUFFER_H_
#define BYTEBUFFER_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct
{
	uint8_t * /*const */ buf;
	/* const */ size_t size;
	size_t in;
	size_t out;
} ByteBuffer_t;


#ifdef __cplusplus
extern "C" {
#endif

ByteBuffer_t ByteBuffer_initial(uint8_t *buf, size_t size);
void ByteBuffer_init(ByteBuffer_t *self, uint8_t *buf, size_t size);
bool ByteBuffer_empty(const ByteBuffer_t *self);
bool ByteBuffer_full(const ByteBuffer_t *self);
void ByteBuffer_in(ByteBuffer_t *self, uint8_t value);
uint8_t ByteBuffer_out(ByteBuffer_t *self);
void ByteBuffer_flush(ByteBuffer_t *self);

#ifdef __cplusplus
}
#endif

#endif /* BYTEBUFFER_H_ */
