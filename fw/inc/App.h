/**
 * @file	App.h
 * @brief	
 * @date	Jun 29, 2014
 * @author	jenda
 */

#ifndef APP_H_
#define APP_H_

#ifdef __cplusplus
extern "C" {
#endif
int app_debug_gsm(const char *fmt, ...);
void app_fatal_error(const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* APP_H_ */
