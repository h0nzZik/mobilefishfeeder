/**
 * @file	pinout.h
 * @brief	
 * @date	Jun 17, 2014
 * @author	jenda
 */

#ifndef PINOUT_H_
#define PINOUT_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


void pin_gsm_term_on(bool value);

void pin_gsm_reset(bool value);

void pinout_init(void);
void pin_gsm_term_on_setup(void);

#ifdef __cplusplus
}
#endif


#endif /* PINOUT_H_ */
